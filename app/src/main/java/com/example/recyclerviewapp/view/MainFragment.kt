package com.example.recyclerviewapp.view

import android.content.res.Resources
import android.graphics.Color
import android.graphics.Color.GREEN
import android.graphics.ColorSpace
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewapp.databinding.FragmentMainBinding
import com.example.recyclerviewapp.view.adapter.AdaptyAdapter
import com.example.recyclerviewapp.viewmodels.MainViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding get() = _binding!!

    private val viewmodel by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentMainBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
//        binding.generateColorButton.setOnClickListener {
//            Log.d("MainFragment", "clicking")
//
//        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initObservers() = with(viewmodel) {
        colorList.observe(viewLifecycleOwner) { card ->
            binding.generateColorButton.setOnClickListener {
                Log.d("MainFragment", "clicking")
                binding.colorRecyclerview.layoutManager = LinearLayoutManager(requireContext())
                binding.colorRecyclerview.adapter = AdaptyAdapter().apply {
                    setData(card.colors)
                }
            }
        }
    }

}