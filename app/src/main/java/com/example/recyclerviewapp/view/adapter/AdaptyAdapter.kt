package com.example.recyclerviewapp.view.adapter

import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewapp.databinding.CardColorBinding

class AdaptyAdapter : RecyclerView.Adapter<AdaptyAdapter.ColorViewHolder>() {

    private lateinit var data: List<Color>

    class ColorViewHolder(private val binding: CardColorBinding) : RecyclerView.ViewHolder(binding.root) {
        @RequiresApi(Build.VERSION_CODES.O)
        fun apply(color: Color) {
            binding.colorWall.setBackgroundColor(color.toArgb())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val binding = CardColorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ColorViewHolder(binding)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setData(data: List<Color>) {
        Log.d("log", "setting data \n ${data.toString()}")
        this.data = data
    }

}