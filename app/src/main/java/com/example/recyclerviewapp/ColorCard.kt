package com.example.recyclerviewapp

import android.graphics.Color

data class ColorCard(var colors: List<Color>)