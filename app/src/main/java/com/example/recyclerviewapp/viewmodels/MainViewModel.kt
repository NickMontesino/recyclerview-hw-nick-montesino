package com.example.recyclerviewapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.recyclerviewapp.ColorCard
import com.example.recyclerviewapp.model.MainRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val repo by lazy { MainRepo }

    private val _colorList: MutableLiveData<ColorCard> = MutableLiveData(ColorCard(listOf()))
    val colorList: LiveData<ColorCard> get() = _colorList

    init {
        viewModelScope.launch(Dispatchers.Main) {
            val response= repo.fetchColorsFromAPI()
            _colorList.value = response
        }
    }

}