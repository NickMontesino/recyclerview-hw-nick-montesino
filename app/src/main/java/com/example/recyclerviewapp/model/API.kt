package com.example.recyclerviewapp.model

import android.graphics.Color

interface API {
    suspend fun fetchColors(): List<Color>
}