package com.example.recyclerviewapp.model

import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.recyclerviewapp.ColorCard
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object MainRepo {

    private val remoteAPI = object : API {
        @RequiresApi(Build.VERSION_CODES.O)
        override suspend fun fetchColors(): List<Color> {
            val colors: MutableList<Color> = mutableListOf()
            for (i in 0..25) {
                colors += Color.valueOf(Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255)))
            }
            return colors
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun fetchColorsFromAPI() = withContext(Dispatchers.IO) {
        delay(3000L)
        return@withContext ColorCard(colors = remoteAPI.fetchColors())
    }

}